import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import Button from 'material-ui/Button'
import Divider from 'material-ui/Divider/Divider'
import Grid from 'material-ui/Grid'
import IconButton from 'material-ui/IconButton'
import List, { ListItem, ListItemText } from 'material-ui/List'
import ListItemSecondaryAction from 'material-ui/List/ListItemSecondaryAction'
import TextField from 'material-ui/TextField'

import DeleteIcon from 'material-ui-icons/Delete'

import { EnhancedAutoSuggest, Loader, Section } from '../../components'
import { removeSuggestion } from '../../actions/autosuggest'
import { ENDPOINTS, fetchJson, submit } from '../../actions/rest'

const projectDetailsKey = 'projectDetails'

class ProjectDetails extends Component {

    constructor(props) {
        super(props)

        this.handleChange = this.handleChange.bind(this)
    }

    state = {
        data: {},
        assignedApprovers: [],
        selectedApprovers: [],
        clients: [],
        loading: true
    }

    componentDidMount() {
        const projectId = this.props.match.params.id

        if (projectId !== 'new') {
            this.props.fetchById(projectId)
            this.props.fetchAssignedApprovers(projectId)
        } else {
            this.setState({ loading: false })
        }

        this.props.fetchClients()
    }

    componentWillReceiveProps(props) {
        const rest = props.rest[projectDetailsKey]
        if (rest && rest.data) {
            this.setState({ ...rest })
        }

        const clientsRest = props.rest['projectClientsList']
        if (clientsRest && clientsRest.items) {
            this.setState({
                clients: clientsRest.items,
                loading: clientsRest.loading
            })
        }

        const assignedApproversRest = props.rest['assignedApprovers']
        if (assignedApproversRest && assignedApproversRest.items) {
            this.setState({
                assignedApprovers: assignedApproversRest.items,
                loading: assignedApproversRest.loading
            })
        }

        if (props.autoSuggest.selectedApprovers) {
            this.setState({
                selectedApprovers: props.autoSuggest.selectedApprovers
            })
        }
    }

    handleChange(event) {
        this.setState({
            data: {
                ...this.state.data,
                [event.target.name]: event.target.value
            }
        })
    }

    render() {
        if (this.state.loading && this.props.match.params.id !== 'new') {
            return (
                <Loader />
            )
        }

        const { clientId, projectName, projectCode, description } = this.state.data
        const { clients, assignedApprovers, selectedApprovers } = this.state
        const spacing = 40

        return (
            <Section headline="Project Details">
                <Grid container justify="left" spacing={Number(spacing)}>
                    <Grid item xs={12} md={6} lg={3} xl={2}>
                        <TextField
                            select
                            name="clientId"
                            label="Client Name"
                            value={clientId}
                            onChange={this.handleChange}
                            SelectProps={{
                                native: true
                            }}
                            fullWidth
                            margin="normal">
                            <option defaultValue="0">- Select client -</option>
                            {clients.map(client => (
                                <option key={client.clientId} value={client.clientId}>
                                    {client.clientName}
                                </option>
                            ))}
                        </TextField>
                        <TextField
                            name="projectCode"
                            label="Project Code"
                            margin="normal"
                            fullWidth
                            value={projectCode || ''}
                            onChange={this.handleChange}
                        />
                        <TextField
                            name="projectName"
                            label="Project Name"
                            margin="normal"
                            fullWidth
                            value={projectName || ''}
                            onChange={this.handleChange}
                        />
                        <TextField
                            name="description"
                            label="Description"
                            margin="normal"
                            fullWidth
                            multiline
                            rows="4"
                            value={description || ''}
                            onChange={this.handleChange}
                        />
                    </Grid>
                    <Grid item xs={12} md={6} lg={3} xl={2}>
                        <EnhancedAutoSuggest autoSuggestKey="selectedApprovers"
                            autoSuggestValueKey="fullName"
                            autoSuggestLabel="Approvers"
                            selectedSuggestions={assignedApprovers}
                            url={`${ENDPOINTS.USERS}/approvers`} />
                        <List>
                            {selectedApprovers.map((user, index) => (
                                <ListItem key={index}>
                                    <ListItemText primary={user.fullName} />
                                    <ListItemSecondaryAction>
                                        <IconButton onClick={() => this.props.onClickRemoveUser(user.userId)}>
                                            <DeleteIcon />
                                        </IconButton>
                                    </ListItemSecondaryAction>
                                </ListItem>
                            ))}
                        </List>
                    </Grid>
                </Grid>
                <Divider style={{ margin: '20px 0px' }} />
                <div>
                    <Button raised onClick={() => this.props.onClickSave(this.state.data, selectedApprovers)}>
                        SAVE
                    </Button>
                    <Link to="/project/list/">
                        <Button raised>
                            CANCEL
                        </Button>
                    </Link>
                </div>
            </Section>
        )
    }
}

const mapStateToProps = (state) => ({
    autoSuggest: state.autoSuggest,
    rest: state.rest
})

const mapDispatchToProps = (dispatch) => {
    return {
        fetchById: (id) => dispatch(
            fetchJson({ key: projectDetailsKey, url: `${ENDPOINTS.PROJECTS}/${id}` })
        ),
        fetchClients: () => dispatch(
            fetchJson({ key: 'projectClientsList', url: `${ENDPOINTS.CLIENTS}` })
        ),
        fetchAssignedApprovers: (projectId) => dispatch(
            fetchJson({
                key: 'assignedApprovers',
                url: `${ENDPOINTS.PROJECTS}/${projectId}/approvers`
            })
        ),
        onClickSave: (data, approvers) => {
            data.approvers = approvers.map(approver => approver.userId)
            dispatch(
                submit({
                    key: projectDetailsKey,
                    url: ENDPOINTS.PROJECTS,
                    method: data.projectId ? 'PUT' : 'POST',
                    data,
                    chain: fetchJson({
                        key: 'assignedApprovers',
                        url: `${ENDPOINTS.PROJECTS}/${data.projectId}/approvers`
                    })
                })
            )
        }, onClickRemoveUser: (userId) => dispatch(
            removeSuggestion({
                key: 'selectedApprovers',
                value: userId,
                valueKey: 'userId'
            })
        )
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProjectDetails)
