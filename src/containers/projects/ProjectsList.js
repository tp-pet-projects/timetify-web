import React, { Component } from 'react'
import { connect } from 'react-redux'

import { EnhancedDialog, EnhancedTable } from '../../components'
import { confirmDeletion } from '../../actions/common'
import { ENDPOINTS, fetchJson } from '../../actions/rest'

const projectsListKey = 'projectsList'
const columns = [
    { id: 'projectId', primaryKey: true },
    { id: 'clientName', label: 'Client' },
    { id: 'projectCode', label: 'Code' },
    { id: 'projectName', label: 'Name' },
    { id: 'description', label: 'Description' }
]

class Projects extends Component {

    state = {
        loading: true,
        items: []
    }

    componentDidMount() {
        this.props.fetchList()
    }

    componentWillReceiveProps(props) {
        const rest = props.rest[projectsListKey]
        if (rest) {
            this.setState({ ...rest })
        }
    }

    render() {
        const { loading, items } = this.state
        return ([
            <EnhancedDialog dialogId="projectsListDialog" key="projects-list-dialog" />,
            <EnhancedTable key="projects-list-table"
                headline="Projects"
                columns={columns}
                loading={loading}
                rows={items}
                onClickDelete={this.props.onClickDelete} />
        ])
    }
}

const mapStateToProps = (state) => ({
    rest: state.rest
})

const mapDispatchToProps = (dispatch) => {
    return {
        fetchList: () => dispatch(
            fetchJson({ key: projectsListKey, url: ENDPOINTS.PROJECTS })
        ),
        onClickDelete: (data) => dispatch(
            confirmDeletion({
                key: projectsListKey,
                dialogId: 'projectsListDialog',
                id: data.projectId,
                data: `${data.projectCode} - ${data.projectName}`,
                url: ENDPOINTS.PROJECTS,
                dispatch: dispatch
            })
        )
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Projects)
