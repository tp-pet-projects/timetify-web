import ProjectDetails from './ProjectDetails'
import ProjectsList from './ProjectsList'

export {
    ProjectDetails,
    ProjectsList
}
