import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import Button from 'material-ui/Button'
import Divider from 'material-ui/Divider/Divider'
import TextField from 'material-ui/TextField'

import { Loader, Section } from '../../components'
import { ENDPOINTS, fetchJson, submit } from '../../actions/rest'

const userDetailsKey = 'userDetails'

class UserDetails extends Component {

    constructor(props) {
        super(props)
        this.handleChange = this.handleChange.bind(this)
    }

    state = {
        data: {},
        loading: true,
        pristine: true
    }

    componentDidMount() {
        const userId = this.props.match.params.id
        if (userId !== 'new') {
            this.props.fetchById(userId)
        }
    }

    componentWillReceiveProps(props) {
        const rest = props.rest[userDetailsKey]
        if (rest.data) {
            this.setState({ ...rest })
        }
    }

    handleChange(event) {
        this.setState({
            data: {
                ...this.state.data,
                [event.target.name]: event.target.value
            },
            pristine: false
        })
    }

    render() {
        if (this.state.loading && this.props.match.params.id !== 'new') {
            return (
                <Loader />
            )
        }

        const { lastName, firstName, email } = this.state.data

        return (
            <Section headline="User Details">
                <TextField
                    name="lastName"
                    label="Last Name"
                    margin="normal"
                    fullWidth
                    value={lastName || ''}
                    onChange={this.handleChange}
                />
                <TextField
                    name="firstName"
                    label="First Name"
                    margin="normal"
                    fullWidth
                    value={firstName || ''}
                    onChange={this.handleChange}
                />
                <TextField
                    name="email"
                    label="Email Address"
                    margin="normal"
                    fullWidth
                    value={email || ''}
                    onChange={this.handleChange}
                />
                <Divider style={{ margin: '20px 0px' }} />
                <div>
                    <Button raised onClick={() => this.props.onClickSave(this.state.data)}>
                        SAVE
                    </Button>
                    <Link to="/user/list/">
                        <Button raised>
                            CANCEL
                        </Button>
                    </Link>
                </div>
            </Section>
        )
    }
}

const mapStateToProps = (state) => ({
    rest: state.rest
})

const mapDispatchToProps = (dispatch) => {
    return {
        fetchById: (id) => dispatch(
            fetchJson({ key: userDetailsKey, url: `${ENDPOINTS.USERS}/${id}` })
        ),
        onClickSave: (data) => {
            let method = data.userId ? 'PUT' : 'POST'
            dispatch(
                submit({ key: userDetailsKey, url: ENDPOINTS.USERS, data, method })
            )
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UserDetails)
