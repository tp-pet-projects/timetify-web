import React, { Component } from 'react'
import { connect } from 'react-redux'

import Grid from 'material-ui/Grid'
import IconButton from 'material-ui/IconButton'
import List, { ListItem, ListItemText } from 'material-ui/List'
import ListItemSecondaryAction from 'material-ui/List/ListItemSecondaryAction'

import AssignIcon from 'material-ui-icons/AssignmentInd'
import DeleteIcon from 'material-ui-icons/Delete'

import { EnhancedAutoSuggest, EnhancedDialog, EnhancedTable } from '../../components'
import { confirmDeletion, toggleDialog } from '../../actions/common'
import { ENDPOINTS, fetchJson } from '../../actions/rest'
import { assignUserToClients } from '../../actions/users'

import store from '../../store'

const usersListKey = 'usersList'
const columns = [
    { id: 'userId', label: 'User ID', primaryKey: true },
    { id: 'lastName', label: 'Last Name' },
    { id: 'firstName', label: 'First Name' },
    { id: 'email', label: 'Email' }
]

class Users extends Component {

    constructor(props) {
        super(props)
        this.onClickShowAssignDialog = this.onClickShowAssignDialog.bind(this)
    }

    state = {
        items: [],
        loading: true,
        userAssignedClients: [],
        selectedClients: []
    }

    onClickShowAssignDialog(userId) {
        const storeState = store.getState()

        if (storeState.rest.userAssignedClients) {
            storeState.rest['userAssignedClients'].items = []
        }

        if (storeState.common.autoSuggest) {
            storeState.common.autoSuggest.selectedClients = []
        }

        this.props.onClickAssign(userId)
    }

    componentDidMount() {
        this.props.fetchList()
    }

    componentWillReceiveProps(props) {
        const rest = props.rest[usersListKey]
        if (rest) {
            this.setState({ ...rest })
        }

        if (props.autoSuggest.selectedClients) {
            this.setState({ selectedClients: props.autoSuggest.selectedClients })
        }

        const userClientsRest = props.rest['userAssignedClients']

        if (userClientsRest && userClientsRest.items) {
            this.setState({ userAssignedClients: userClientsRest.items })
        }
    }

    render() {
        const { loading, items } = this.state
        return ([
            <EnhancedDialog dialogId="usersListDialog" key="users-list-dialog" />,
            <EnhancedDialog dialogId="usersListAssignDialog" key="users-list-assign-dialog"
                description="Usu ei nusquam sapientem, eu utamur sensibus dissentiunt vim. Ad quo ferri accusata suavitate.">
                <Grid container direction="row" justify="space-between" alignItems="flex-start" style={{ minHeight: 330 }}>
                    <Grid item xs={12} lg={6}>
                        <EnhancedAutoSuggest autoSuggestKey="selectedClients"
                            autoSuggestValueKey="clientName"
                            autoSuggestLabel="Assign to Client"
                            url={ENDPOINTS.CLIENTS} />
                    </Grid>
                    <Grid item xs={12} lg={6}>
                        <List>
                            {this.state.userAssignedClients.map((client, index) => (
                                <ListItem key={index}>
                                    <ListItemText primary={client.clientName} />
                                    <ListItemSecondaryAction>
                                        <IconButton>
                                            <DeleteIcon />
                                        </IconButton>
                                    </ListItemSecondaryAction>
                                </ListItem>
                            ))}
                            {this.state.selectedClients.map((client, index) => (
                                <ListItem key={index}>
                                    <ListItemText primary={client.clientName} />
                                    <ListItemSecondaryAction>
                                        <IconButton>
                                            <DeleteIcon />
                                        </IconButton>
                                    </ListItemSecondaryAction>
                                </ListItem>
                            ))}
                        </List>
                    </Grid>
                </Grid >
            </EnhancedDialog >,
            <EnhancedTable key="users-list-table"
                headline="Users"
                columns={columns}
                loading={loading}
                rows={items}
                onClickDelete={this.props.onClickDelete}
                customActions={[{
                    icon: <AssignIcon />,
                    label: 'Assign',
                    onClick: (userId) => this.onClickShowAssignDialog(userId)
                }]}
            />
        ])
    }
}

const mapStateToProps = (state) => ({
    autoSuggest: state.common.autoSuggest,
    rest: state.rest
})

const mapDispatchToProps = (dispatch) => {
    return {
        fetchList: () => dispatch(
            fetchJson({ key: usersListKey, url: ENDPOINTS.USERS })
        ),
        onClickAssign: (userId) => {
            dispatch(fetchJson({ key: 'userAssignedClients', url: `${ENDPOINTS.USERS}/${userId}/clients` }))
            dispatch(
                toggleDialog({
                    dialogId: 'usersListAssignDialog',
                    open: true,
                    title: 'Assign to client',
                    okCallback: () => {
                        return dispatch(assignUserToClients(userId))
                    }
                })
            )
        },
        onClickDelete: (data) => dispatch(
            confirmDeletion({
                key: usersListKey,
                dialogId: 'usersListDialog',
                id: data.userId,
                data: `${data.lastName}, ${data.firstName}`, url: ENDPOINTS.USERS,
                dispatch: dispatch
            })
        )
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Users)
