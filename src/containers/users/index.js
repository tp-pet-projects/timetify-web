import UserDetails from './UserDetails'
import UsersList from './UsersList'

export {
    UserDetails,
    UsersList
}
