import React, { Component } from 'react'
import { connect } from 'react-redux'

import { withStyles } from 'material-ui/styles'

import Button from 'material-ui/Button'
import Card, { CardActions, CardContent } from 'material-ui/Card'
import TextField from 'material-ui/TextField'
import Typography from 'material-ui/Typography/Typography'

import { authenticate } from '../../actions/rest'

const styles = theme => ({
    card: {
        margin: '100px auto 0px',
        width: '30%',
        [theme.breakpoints.down('sm')]: {
            marginTop: '30px',
            padding: '0px 10px 0px 10px',
            width: '90%'
        },
        [theme.breakpoints.up('sm')]: {
            padding: '0px 10px 0px 10px',
            width: '70%'
        },
        [theme.breakpoints.up('md')]: {
            width: '35%'
        },
        [theme.breakpoints.up('xl')]: {
            width: '20%'
        }
    },
    cardHeadline: {
        // marginTop: '20'
    },
    textField: {
        marginTop: '20px'
    }
})

class SignIn extends Component {

    state = {
        data: {
            email: 'john.doe@domain.com',
            password: '12345'
        }
    }

    constructor(props) {
        super(props)

        this.handleChange = this.handleChange.bind(this)
        this.handleOnClickSignIn = this.handleOnClickSignIn.bind(this)
    }

    handleChange(event) {
        this.setState({
            data: {
                ...this.state.data,
                [event.target.name]: event.target.value
            }
        })
    }

    handleOnClickSignIn() {
        this.props.dispatch(authenticate(this.state.data))
    }

    render() {
        const { classes } = this.props
        const { email, password } = this.state.data

        return (
            <div className={classes.card}>
                <Card elevation={0}>
                    <CardContent>
                        <Typography type="headline" className={classes.cardHeadline}>
                            Welcome | <strong>Sign in</strong>
                        </Typography>
                        <TextField
                            fullWidth
                            name="email"
                            label="Email"
                            value={email || ''}
                            onChange={this.handleChange}
                            className={classes.textField}
                            helperText="jane.doe@domain.com:12345"
                        />
                        <TextField
                            fullWidth
                            name="password"
                            label="Password"
                            value={password || ''}
                            type="password"
                            onChange={this.handleChange}
                            className={classes.textField}
                        />
                    </CardContent>
                    <CardActions>
                        <Button type="submit" raised color="primary"
                            onClick={this.handleOnClickSignIn}>
                            Sign In
                    </Button>
                    </CardActions>
                </Card>
            </div>
        )
    }
}

export default connect()(withStyles(styles, {
    withTheme: true
})(SignIn))

