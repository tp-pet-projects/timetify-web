import React from 'react'
import { NavLink } from 'react-router-dom'
import { ListItem, ListItemIcon, ListItemText } from 'material-ui/List'

const MenuItems = ({ menus = [] }) => {
    return (
        <div>
            {menus.filter((menu) => (menu.navItem))
                .map((menu, index) => (
                    <NavLink to={menu.path} key={index} style={{ textDecoration: 'none' }}>
                        <ListItem button>
                            <ListItemIcon>
                                {menu.icon}
                            </ListItemIcon>
                            <ListItemText primary={menu.label} />
                        </ListItem>
                    </NavLink>
                ))}
        </div>
    )
}

export default MenuItems
