import React, { Component } from 'react'
import { connect } from 'react-redux'

import { EnhancedDialog, EnhancedTable } from '../../components'
import { confirmDeletion } from '../../actions/common'
import { ENDPOINTS, fetchJson } from '../../actions/rest'

const clientsListKey = 'clientsList'
const columns = [
    { id: 'clientId', primaryKey: true },
    { id: 'clientName', label: 'Name' },
    { id: 'address', label: 'Address' },
    { id: 'email', label: 'Email' },
    { id: 'phoneNo', label: 'Phone No' }
]

class ClientsList extends Component {

    state = {
        loading: true,
        items: []
    }

    componentDidMount() {
        this.props.fetchList()
    }

    componentWillReceiveProps(props) {
        const rest = props.rest[clientsListKey]
        if (rest) {
            this.setState({ ...rest })
        }
    }

    render() {
        const { loading, items } = this.state
        return ([
            <EnhancedDialog dialogId="clientsListDialog" key="clients-list-dialog" />,
            <EnhancedTable key="clients-list-table"
                headline="Clients"
                columns={columns}
                loading={loading}
                rows={items}
                onClickDelete={this.props.onClickDelete} />
        ])
    }
}

const mapStateToProps = (state) => ({
    rest: state.rest
})

const mapDispatchToProps = (dispatch) => {
    return {
        fetchList: () => dispatch(
            fetchJson({ key: clientsListKey, url: ENDPOINTS.CLIENTS })
        ),
        onClickDelete: (data) => dispatch(
            confirmDeletion({
                key: clientsListKey,
                dialogId: 'clientsListDialog',
                id: data.clientId,
                data: data.clientName,
                url: ENDPOINTS.CLIENTS,
                dispatch: dispatch
            })
        )
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ClientsList)
