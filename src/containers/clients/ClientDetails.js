import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import Button from 'material-ui/Button'
import Divider from 'material-ui/Divider/Divider'
import TextField from 'material-ui/TextField'

import { Loader, Section } from '../../components'
import { ENDPOINTS, fetchJson, submit } from '../../actions/rest'

const clientDetailsKey = 'clientDetails'

class ClientDetails extends Component {

    constructor(props) {
        super(props)
        this.handleChange = this.handleChange.bind(this)
    }

    state = {
        data: {},
        loading: true,
        pristine: true
    }

    componentDidMount() {
        const clientId = this.props.match.params.id
        if (clientId !== 'new') {
            this.props.fetchById(clientId)
        }
    }

    componentWillReceiveProps(props) {
        const rest = props.rest[clientDetailsKey]
        if (rest && rest.data) {
            this.setState({ ...rest })
        }
    }

    handleChange(event) {
        this.setState({
            data: {
                ...this.state.data,
                [event.target.name]: event.target.value
            },
            pristine: false
        })
    }

    render() {
        if (this.state.loading && this.props.match.params.id !== 'new') {
            return (
                <Loader />
            )
        }

        const { clientName, email, phoneNo, address } = this.state.data

        return (
            <Section headline="Client Details">
                <TextField
                    name="clientName"
                    label="Client Name"
                    margin="normal"
                    fullWidth
                    value={clientName || ''}
                    onChange={this.handleChange}
                />
                <TextField
                    name="email"
                    label="Email Address"
                    margin="normal"
                    fullWidth
                    value={email || ''}
                    onChange={this.handleChange}
                />
                <TextField
                    name="phoneNo"
                    label="Phone Number"
                    margin="normal"
                    fullWidth
                    value={phoneNo || ''}
                    onChange={this.handleChange}
                />
                <TextField
                    name="address"
                    label="Address"
                    margin="normal"
                    fullWidth
                    multiline
                    rows="4"
                    value={address || ''}
                    onChange={this.handleChange}
                />

                <Divider style={{ margin: '20px 0px' }} />
                <div>
                    <Button raised onClick={() => this.props.onClickSave(this.state.data)}>
                        SAVE
                    </Button>
                    <Link to="/client/list/">
                        <Button raised>
                            CANCEL
                        </Button>
                    </Link>
                </div>
            </Section>
        )
    }
}

const mapStateToProps = (state) => ({
    rest: state.rest
})

const mapDispatchToProps = (dispatch) => {
    return {
        fetchById: (id) => dispatch(
            fetchJson({ key: clientDetailsKey, url: `${ENDPOINTS.CLIENTS}/${id}` })
        ),
        onClickSave: (data) => {
            let method = data.clientId ? 'PUT' : 'POST'
            dispatch(
                submit({ key: clientDetailsKey, url: ENDPOINTS.CLIENTS, data, method })
            )
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ClientDetails)
