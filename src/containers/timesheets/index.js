import React, { Component } from 'react'
import { connect } from 'react-redux'
import moment from 'moment'

import Button from 'material-ui/Button'
import Divider from 'material-ui/Divider/Divider'
import Grid from 'material-ui/Grid'
import TextField from 'material-ui/TextField'
import Typography from 'material-ui/Typography/Typography'

import { WeekPicker } from '../../components'
import { ENDPOINTS, fetchJson, submit } from '../../actions/rest'
import '../../styles/_responsive_table.css'

const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']

class Timesheets extends Component {

    constructor(props) {
        super(props)

        this.handleHoursChange = this.handleHoursChange.bind(this)
        this.onChangeClient = this.onChangeClient.bind(this)
        this.onChangeProject = this.onChangeProject.bind(this)

        this.initTimesheetData = this.initTimesheetData.bind(this)
    }

    state = {
        clientId: 0,
        projectId: 0,
        assignedClients: [],
        clientProjects: [],
        timesheets: {},
        periodCovered: moment(),
        loggedTimesheets: []
    }

    initTimesheetData() {
        const { clientProjects, loggedTimesheets, periodCovered } = this.state
        let timesheets = {}

        for (let index = 0; index < clientProjects.length; index++) {
            const projectId = clientProjects[index].projectId
            const filteredTimesheets = loggedTimesheets.filter(loggedTimesheet => loggedTimesheet.projectId === projectId)

            if (filteredTimesheets.length > 0) {
                const loggedTimesheet = filteredTimesheets[0]
                const dates = {}

                days.forEach(day => {
                    const currentDate = periodCovered.day(day).format('DD-MM-YYYY')
                    const loggedHours = loggedTimesheet.timesheetLogs
                        .filter(log => log.logDate === currentDate)

                    dates[currentDate] = {}

                    if (loggedHours.length > 0) {
                        const _tmp = loggedHours[0]
                        dates[currentDate] = {
                            timesheetLogId: _tmp.timesheetLogId,
                            logDate: _tmp.logDate,
                            logHours: _tmp.logHours
                        }
                    } else {
                        dates[currentDate] = {
                            logDate: currentDate,
                            logHours: ''
                        }
                    }
                })

                timesheets[`timesheet_${index}`] = {
                    timesheetId: loggedTimesheet.timesheetId,
                    timesheetStatus: loggedTimesheet.timesheetStatus,
                    projectId,
                    dates: dates
                }
            } else {
                const dates = {}
                days.forEach(day => {
                    const currentDate = periodCovered.day(day).format('DD-MM-YYYY')
                    dates[currentDate] = {
                        logDate: currentDate,
                        logHours: ''
                    }
                })

                timesheets[`timesheet_${index}`] = {
                    timesheetStatus: 'DRAFT',
                    projectId: 0,
                    dates: dates
                }
            }
        }

        this.setState({ timesheets: timesheets })
    }

    componentDidMount() {
        this.props.fetchAssignedClients()
    }

    componentWillReceiveProps(props) {
        const clients = props.rest['assignedClients']
        if (clients && clients.items) {
            this.setState({
                assignedClients: clients.items
            })
        }

        const projectsRest = props.rest['clientProjects']
        if (projectsRest && projectsRest.items) {
            this.setState({
                clientProjects: projectsRest.items
            })

            if (!props.weekPicker || !props.weekPicker.periodCovered) {
                this.initTimesheetData()
            }
        }

        const timesheetsRest = props.rest['loggedTimesheets']
        if (timesheetsRest && timesheetsRest.items) {
            this.setState({
                loggedTimesheets: timesheetsRest.items
            })

            if (!props.weekPicker || !props.weekPicker.periodCovered) {
                this.initTimesheetData()
            }
        }

        const weekPicker = props.weekPicker
        if (weekPicker && weekPicker.periodCovered) {
            const currentPeriodCovered = this.state.periodCovered.clone().startOf('week').format('DD-MM-YYYY')
            const newPeriodCovered = weekPicker.periodCovered.clone().startOf('week').format('DD-MM-YYYY')

            this.setState({
                periodCovered: weekPicker.periodCovered.clone()
            }, function () {
                if (currentPeriodCovered !== newPeriodCovered) {
                    this.props.fetchTimesheets(this.state.clientId, weekPicker.periodCovered.clone())
                } else {
                    this.initTimesheetData()
                }
            }.bind(this))
        }
    }

    handleHoursChange(event, timesheetKey) {
        const timesheetsNextState = this.state.timesheets
        timesheetsNextState[timesheetKey].dates[event.target.name].logHours = event.target.value
        this.setState({ timesheets: timesheetsNextState })
    }

    onChangeClient(event) {
        const clientId = event.target.value

        this.setState({
            clientId: clientId,
        })

        if (clientId > 0) {
            this.props.fetchClientProjects(clientId, this.state.periodCovered)
        } else {
            this.setState({
                clientProjects: []
            })
            this.initProjectsData()
        }
    }

    onChangeProject(event, timesheetKey) {
        const timesheetsNextState = this.state.timesheets
        timesheetsNextState[timesheetKey].projectId = event.target.value
        this.setState({
            timesheets: timesheetsNextState
        })
    }

    render() {
        const { clientId, clientProjects, assignedClients, timesheets, periodCovered } = this.state
        const timesheetsKeys = Object.keys(timesheets)
        return (
            <div>
                <h3>Timesheets</h3>

                <Grid container alignItems="center" direction="row" justify="space-between" >
                    <Grid item>
                        <TextField
                            select
                            name="clientId"
                            label="Client Name"
                            value={this.state.clientId}
                            onChange={this.onChangeClient}
                            SelectProps={{
                                native: true
                            }}
                            margin="normal">
                            <option defaultValue="0">- Select client -</option>
                            {assignedClients.map(client => (
                                <option key={client.clientId} value={client.clientId}>
                                    {client.clientName}
                                </option>
                            ))}
                        </TextField>
                    </Grid>
                    <Grid item>
                        <WeekPicker pickerKey="periodCovered" />
                    </Grid>
                </Grid>
                <table>
                    <thead>
                        <tr>
                            <th scope="col">
                                <Typography type="subheading">Project</Typography>
                            </th>
                            {days.map((day, index) => (
                                <th scope="col" key={index}>
                                    <Typography type="subheading">{day}</Typography>
                                    <Typography>{periodCovered.day(day).format('MMM DD')}</Typography>
                                </th>
                            ))}
                        </tr>
                    </thead>
                    <tbody>
                        {timesheetsKeys.length === 0 ?
                            <tr>
                                <td colSpan={days.length + 1}>
                                    <Typography type="subheading">Select client first</Typography>
                                </td>
                            </tr> :
                            timesheetsKeys.map((timesheetKey) => {
                                const timesheet = timesheets[timesheetKey]
                                const disabled = timesheet.timesheetStatus === 'SUBMITTED' ? true : false

                                return <tr key={timesheetKey}>
                                    <td data-label="Project">
                                        <TextField
                                            select
                                            disabled={disabled}
                                            value={timesheet.projectId}
                                            onChange={e => this.onChangeProject(e, timesheetKey)}
                                            SelectProps={{
                                                native: true
                                            }}
                                            margin="normal"
                                            fullWidth>
                                            <option defaultValue="0">- Select project -</option>
                                            {clientProjects.map(project => (
                                                <option key={project.projectId} value={project.projectId}>
                                                    {`${project.projectCode} - ${project.projectName}`}
                                                </option>
                                            ))}
                                        </TextField>
                                    </td>
                                    {days.map((day, index) => {
                                        const logDate = periodCovered.day(day).format('DD-MM-YYYY')
                                        let logHours

                                        if (timesheet.dates[logDate]) {
                                            logHours = timesheet.dates[logDate].logHours
                                        }

                                        return <td key={index} data-label={day}>
                                            <TextField
                                                label="Hours"
                                                margin="normal"
                                                name={logDate}
                                                value={logHours || ''}
                                                disabled={disabled}
                                                onChange={e => this.handleHoursChange(e, timesheetKey)}
                                            />
                                        </td>
                                    })}
                                </tr>
                            })}
                    </tbody>
                </table>
                <Divider style={{ margin: '20px 0px' }} />
                <div>
                    <Button raised onClick={() => {
                        this.props.onClickSubmit('save', timesheets, clientId, periodCovered)
                    }}>
                        SAVE
                    </Button>
                    <Button raised onClick={() => {
                        this.props.onClickSubmit('submit', timesheets, clientId, periodCovered)
                    }}>
                        SUBMIT
                    </Button>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    rest: state.rest,
    weekPicker: state.common.weekPicker
})

const mapDispatchToProps = (dispatch) => {
    return {
        fetchAssignedClients: () => {
            dispatch(fetchJson({
                key: 'assignedClients', url: `${ENDPOINTS.PROFILE}/clients`
            }))
        },
        fetchClientProjects: (clientId, periodCovered) => {
            const start = periodCovered.clone().startOf('week').format('DD-MM-YYYY')
            const end = periodCovered.clone().endOf('week').format('DD-MM-YYYY')

            dispatch(fetchJson({
                key: 'clientProjects',
                url: `${ENDPOINTS.CLIENTS}/${clientId}/projects`,
                chain: fetchJson({
                    key: 'loggedTimesheets',
                    url: `${ENDPOINTS.TIMESHEET}/logs/${clientId}?start=${start}&end=${end}`
                })
            }))
        },
        fetchTimesheets: (clientId, periodCovered) => {
            const start = periodCovered.clone().startOf('week').format('DD-MM-YYYY')
            const end = periodCovered.clone().endOf('week').format('DD-MM-YYYY')

            dispatch(fetchJson({
                key: 'loggedTimesheets',
                url: `${ENDPOINTS.TIMESHEET}/logs/${clientId}?start=${start}&end=${end}`
            }))
        },
        onClickSubmit: (action, timesheets, clientId, periodCovered) => {
            const start = periodCovered.clone().startOf('week').format('DD-MM-YYYY')
            const end = periodCovered.clone().endOf('week').format('DD-MM-YYYY')

            console.log('timesheets', timesheets)

            Object.keys(timesheets)
                .filter(timesheetKey => timesheets[timesheetKey].projectId > 0 && timesheets[timesheetKey].timesheetStatus === 'DRAFT')
                .forEach(timesheetKey => {
                    const timesheet = timesheets[timesheetKey]
                    const logDates = timesheet.dates
                    const timesheetLogs = Object.keys(logDates)
                        .filter(logDate => logDates[logDate].logHours > 0)
                        .map(logDate => logDates[logDate])

                    const data = {
                        timesheetId: timesheet.timesheetId,
                        projectId: timesheet.projectId,
                        timesheetLogs: timesheetLogs
                    }

                    dispatch(submit({
                        key: 'loggedTimesheets',
                        method: 'POST',
                        data: data,
                        url: `${ENDPOINTS.TIMESHEET}/${action}`,
                        chain: fetchJson({
                            key: 'loggedTimesheets',
                            url: `${ENDPOINTS.TIMESHEET}/logs/${clientId}?start=${start}&end=${end}`
                        })
                    }))
                })
        }
    }
}


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Timesheets)
