import React, { Component } from 'react'
import { connect } from 'react-redux'
import { NavLink, Route, Switch } from 'react-router-dom'

import { withStyles } from 'material-ui/styles'

import AppBar from 'material-ui/AppBar'
import Button from 'material-ui/Button'
import Snackbar from 'material-ui/Snackbar'
import Toolbar from 'material-ui/Toolbar'
import Typography from 'material-ui/Typography'

import { hideSnackbar } from '../../actions/snackbar'
import { menus } from '../../routes/public'

const styles = {
    root: {
        width: '100%',
    },
    flex: {
        flex: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    }
}

const ConnectedSwitch = connect(state => ({
    location: state.routerReducer.location
}))(Switch)

class PublicLayout extends Component {

    render() {
        const { classes, snackbar, onClickSnackbar } = this.props
        return (
            <div>
                <AppBar position="static">
                    <Toolbar>
                        <Typography type="title" color="inherit" className={classes.flex}>
                            Timetify
                        </Typography>
                        <NavLink to="/sign-in/" color="inherit" style={{ textDecoration: 'none' }}>
                            <Button dense style={{ color: 'white' }}>Sign in</Button>
                        </NavLink>
                        |
                        <NavLink to="/sign-up/" color="inherit" style={{ textDecoration: 'none' }}>
                            <Button dense style={{ color: 'white' }}>Sign up</Button>
                        </NavLink>
                    </Toolbar>
                </AppBar>
                <div>
                    <ConnectedSwitch>
                        {menus.map((menu, index) => (
                            <Route key={index}
                                exact={menu.exact}
                                path={menu.path}
                                component={menu.component} />
                        ))}
                    </ConnectedSwitch>
                    <Snackbar
                        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                        open={snackbar.open}
                        SnackbarContentProps={{
                            'aria-describedby': 'message-id',
                        }}
                        action={
                            <Button color="accent" dense onClick={onClickSnackbar}>
                                Close
                            </Button>
                        }
                        message={<span id="message-id">{snackbar.message}</span>}
                    />
                </div>
                <h6>Footer goes here</h6>
            </div>
        )
    }
}


const mapStateToProps = (state) => ({
    snackbar: state.snackbar
})

const mapDispatchToProps = (dispatch) => {
    return {
        onClickSnackbar: () => dispatch(hideSnackbar())
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles, {
    withTheme: true
})(PublicLayout))
