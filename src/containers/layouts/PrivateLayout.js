import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Route, Switch } from 'react-router-dom'

import classNames from 'classnames'
import { withStyles } from 'material-ui/styles'

import AppBar from 'material-ui/AppBar'
import Button from 'material-ui/Button'
import Drawer from 'material-ui/Drawer'
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List'
import Snackbar from 'material-ui/Snackbar'
import Toolbar from 'material-ui/Toolbar'
import Typography from 'material-ui/Typography'

import Divider from 'material-ui/Divider'
import IconButton from 'material-ui/IconButton'
import MenuIcon from 'material-ui-icons/Menu'
import SignoutIcon from 'material-ui-icons/Input'

import MenuItems from '../MenuItems'
import { hideSnackbar } from '../../actions/snackbar'
import { menus } from '../../routes/private'

const drawerWidth = 240;
const styles = theme => ({
    root: {
        width: '100%',
        height: 430,
        marginTop: theme.spacing.unit * 3,
        zIndex: 1,
        overflow: 'hidden',
    },
    appFrame: {
        position: 'relative',
        display: 'flex',
        width: '100%',
        height: '100%',
    },
    appBar: {
        position: 'absolute'
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 20,
    },
    drawerPaper: {
        height: '100%',
        width: drawerWidth,
        marginTop: '65px'
    },
    content: {
        width: '100%',
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing.unit * 3,
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        // height: 'calc(100% - 56px)',
        height: '85vh',
        marginTop: 65,
        [theme.breakpoints.up('sm')]: {
            content: {
                height: 'calc(100% - 64px)',
                marginTop: 64,
            },
        },
    },
    'content-left': {
        marginLeft: 0,
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    'contentShift-left': {
        marginLeft: drawerWidth + 1,
    }
})

const ConnectedSwitch = connect(state => ({
    location: state.routerReducer.location
}))(Switch)

class PrivateLayout extends Component {

    state = {
        open: true,
        anchor: 'left'
    }

    onClickToggleDrawer = () => {
        this.setState({ open: !this.state.open })
    }

    handleDrawerClose = () => {
        this.setState({ open: false })
    }

    handleChangeAnchor = event => {
        this.setState({
            anchor: event.target.value,
        })
    }

    isAuthorized(userRoles, menuRoles) {
        let ctr = 0
        menuRoles.forEach(role => {
            if (userRoles.includes(role)) {
                ctr++
            }
        })

        return ctr > 0
    }

    signout() {
        localStorage.setItem('token', '')
        window.location.href = '/'
    }

    render() {
        const { classes, snackbar, onClickSnackbar, roles } = this.props
        const { anchor, open } = this.state

        const filteredMenus = menus.filter(_menu => this.isAuthorized(roles, _menu.roles))

        const drawer = <Drawer
            type="persistent"
            classes={{
                paper: classes.drawerPaper,
            }}
            anchor={anchor}
            open={open} >
            <div className={classes.drawerInner}>
                <List className={classes.list}>
                    <MenuItems menus={filteredMenus} />
                </List>
                <Divider />
                <List className={classes.list}>
                    <ListItem button onClick={() => this.signout()}>
                        <ListItemIcon>
                            <SignoutIcon />
                        </ListItemIcon>
                        <ListItemText primary="Sign out" />
                    </ListItem>
                </List>
            </div>
        </Drawer>

        return (
            <div className={classes.appFrame}>
                <AppBar>
                    <Toolbar disableGutters={true}>
                        <IconButton
                            color="contrast"
                            aria-label="open drawer"
                            onClick={this.onClickToggleDrawer}
                            className={classNames(classes.menuButton)}>
                            <MenuIcon />
                        </IconButton>
                        <Typography type="title" color="inherit" noWrap>
                            Dashboard...
                        </Typography>
                    </Toolbar>
                </AppBar>
                {drawer}
                <main
                    className={classNames(classes.content, classes[`content-${anchor}`], {
                        [classes.contentShift]: open,
                        [classes[`contentShift-${anchor}`]]: open,
                    })}>
                    <ConnectedSwitch>
                        {filteredMenus.map((menu, index) => (
                            <Route key={index}
                                exact={menu.exact}
                                path={menu.path}
                                component={menu.component} />
                        ))}
                    </ConnectedSwitch>
                </main>
                <Snackbar
                    anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                    open={snackbar.open}
                    SnackbarContentProps={{
                        'aria-describedby': 'message-id',
                    }}
                    action={
                        <Button color="accent" dense onClick={onClickSnackbar}>
                            Close
                        </Button>
                    }
                    message={<span id="message-id">{snackbar.message}</span>}
                />

            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    snackbar: state.snackbar
})

const mapDispatchToProps = (dispatch) => {
    return {
        onClickSnackbar: () => dispatch(hideSnackbar())
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles, {
    withTheme: true
})(PrivateLayout))
