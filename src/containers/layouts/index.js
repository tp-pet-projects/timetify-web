import React, { Component } from 'react'
import { connect } from 'react-redux'

import PrivateLayout from './PrivateLayout'
import PublicLayout from './PublicLayout'
import { ENDPOINTS, fetchJson } from '../../actions/rest'

const menus = []

class AppLayout extends Component {

    state = {
        loading: true,
        data: {}
    }

    componentDidMount() {
        this.props.isAuthenticated()
    }

    componentWillReceiveProps(props) {
        const rest = props.rest['authenticationCheck']
        if (rest) {
            this.setState({
                ...this.state,
                ...rest
            })
        }
    }

    render() {
        const { loading, data } = this.state

        if (loading) {
            return (
                <h3>Please wait...</h3>
            )
        }

        return (
            data.userId ?
                <PrivateLayout roles={data.userRoles} /> :
                <PublicLayout modules={menus} />
        )
    }

}

const mapStateToProps = (state) => ({
    rest: state.rest
})

const mapDispatchToProps = (dispatch) => {
    return {
        isAuthenticated: () => {
            dispatch(fetchJson({
                url: ENDPOINTS.PROFILE,
                key: 'authenticationCheck'
            }))
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AppLayout)
