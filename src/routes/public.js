import SignIn from '../containers/public/SignIn'
import SignUp from '../containers/public/SignUp'

export const menus = [
    {
        path: '/sign-in/',
        component: SignIn,
        exact: true
    }, {
        path: '/sign-up/',
        component: SignUp,
        exact: true
    }
]
