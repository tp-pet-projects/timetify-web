import React from 'react'

import DashboardIcon from 'material-ui-icons/Dashboard'
import TimesheetsIcon from 'material-ui-icons/AccessTime'
import ReportsIcon from 'material-ui-icons/Description'

import ClientsIcon from 'material-ui-icons/People'
import ProjectsIcon from 'material-ui-icons/Assignment'
import UsersIcon from 'material-ui-icons/PersonAdd'

import Dashboard from '../containers/Dashboard'
import Timesheets from '../containers/timesheets'
import Reports from '../containers/reports'

import { ClientDetails, ClientsList } from '../containers/clients'
import { ProjectDetails, ProjectsList } from '../containers/projects'
import { UserDetails, UsersList } from '../containers/users'

const ROLES = {
    ADMIN: 'ROLE_ADMIN',
    USER: 'ROLE_USER'
}

export const menus = [
    {
        path: '/client/list/',
        component: ClientsList,
        label: 'Clients',
        icon: <ClientsIcon />,
        navItem: true,
        exact: true,
        roles: [ROLES.ADMIN]
    }, {
        path: '/client/:id/',
        component: ClientDetails,
        navItem: false,
        exact: true,
        roles: [ROLES.ADMIN]
    }, {
        path: '/project/list/',
        component: ProjectsList,
        label: 'Projects',
        icon: <ProjectsIcon />,
        navItem: true,
        exact: true,
        roles: [ROLES.ADMIN]
    }, {
        path: '/project/:id/',
        component: ProjectDetails,
        navItem: false,
        exact: true,
        roles: [ROLES.ADMIN]
    }, {
        path: '/user/list/',
        component: UsersList,
        label: 'Users',
        icon: <UsersIcon />,
        navItem: true,
        exact: true,
        roles: [ROLES.ADMIN]
    }, {
        path: '/user/:id/',
        component: UserDetails,
        navItem: false,
        exact: true,
        roles: [ROLES.ADMIN]
    }, {
        path: '/reports/',
        component: Reports,
        label: 'Reports',
        icon: <ReportsIcon />,
        navItem: true,
        roles: [ROLES.ADMIN]
    },
    {
        path: '/dashboard/',
        component: Dashboard,
        label: 'Dashboard',
        icon: <DashboardIcon />,
        navItem: true,
        roles: [ROLES.USER]
    }, {
        path: '/timesheet/all/',
        component: Timesheets,
        label: 'Timesheets',
        icon: <TimesheetsIcon />,
        navItem: true,
        roles: [ROLES.USER]
    }
]
