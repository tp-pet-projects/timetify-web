import React from 'react'
import { withStyles } from 'material-ui/styles'
import { LinearProgress } from 'material-ui/Progress'
import Typography from 'material-ui/Typography/Typography';

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
    },
})

const Loader = (props) => {
    const { classes } = props;
    return (
        <div className={classes.root}>
            <Typography type="subheading" gutterBottom>
                Loading...
            </Typography>
            <LinearProgress mode="query" />
        </div>
    )
}

export default withStyles(styles)(Loader)
