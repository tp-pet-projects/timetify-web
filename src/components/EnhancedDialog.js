import React, { Component } from 'react'
import { connect } from 'react-redux'

import Button from 'material-ui/Button'
import Dialog, {
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
} from 'material-ui/Dialog'

import { toggleDialog } from '../actions/common'

class EnhancedDialog extends Component {

    state = {
        dialog: {}
    }

    componentWillReceiveProps(props) {
        const dialog = props[this.props.dialogId]
        if (dialog) {
            if (this.props.description) {
                dialog.description = this.props.description
            }
            this.setState({ dialog: dialog })
        }
    }

    render() {
        const { dialogId, title, description, open, okCallback } = this.state.dialog
        const { children = [], onClickCancel, onClickOk } = this.props

        return (
            <Dialog
                key={dialogId}
                open={open}
                onClose={onClickCancel}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description" >
                <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {description}
                    </DialogContentText>
                    {children}
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => onClickOk(dialogId, okCallback)} color="primary">
                        OK
                    </Button>
                    <Button onClick={() => onClickCancel(dialogId)} color="primary" autoFocus>
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog >
        )
    }
}

const mapStateToProps = (state) => ({
    ...state.common.dialog
})

const mapDispatchToProps = (dispatch) => {
    return {
        onClickCancel: (dialogId) => dispatch(toggleDialog({ dialogId: dialogId })),
        onClickOk: (dialogId, okCallback) => {
            okCallback()
            dispatch(toggleDialog({ dialogId: dialogId }))
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EnhancedDialog)
