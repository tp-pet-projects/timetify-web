import React from 'react'
import Divider from 'material-ui/Divider'
import Paper from 'material-ui/Paper'
import Typography from 'material-ui/Typography'

const Section = ({ children = [], headline }) => {
    return (
        <Paper style={{ padding: 20 }} elevation={4}>
            <Typography type="title" component="h5">
                {headline}
            </Typography>
            <Divider light style={{ margin: '15px 0px' }} />
            {children}
        </Paper>
    )
}

export default Section
