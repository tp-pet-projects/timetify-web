import React, { Fragment, Component } from 'react'
import { connect } from 'react-redux'

import classNames from 'classnames'

import { IconButton, withStyles } from 'material-ui'
import { DatePicker } from 'material-ui-pickers'

import { setSelectedDate } from '../actions/common'

const styles = theme => ({
    dayWrapper: {
        position: 'relative',
    },
    day: {
        width: 36,
        height: 36,
        fontSize: theme.typography.caption.fontSize,
        margin: '0 2px',
        color: 'inherit',
    },
    customDayHighlight: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: '2px',
        right: '2px',
        border: `2px solid ${theme.palette.primary[100]}`,
        borderRadius: '50%',
    },
    nonCurrentMonthDay: {
        color: theme.palette.common.minBlack,
    },
    highlightNonCurrentMonthDay: {
        color: '#676767',
    },
    highlight: {
        background: theme.palette.primary[500],
        color: theme.palette.common.white,
    },
    firstHighlight: {
        extend: 'highlight',
        borderTopLeftRadius: '50%',
        borderBottomLeftRadius: '50%',
    },
    endHighlight: {
        extend: 'highlight',
        borderTopRightRadius: '50%',
        borderBottomRightRadius: '50%',
    },
})

class WeekPicker extends Component {

    state = {
        selectedDate: new Date(),
    }

    handleDateChange = (date) => {
        this.setState({ selectedDate: date })
        this.props.dispatch(setSelectedDate(this.props.pickerKey, date))
    }

    handleWeekChange = (date) => {
        this.setState({ selectedDate: date.clone().startOf('week') })
    }

    formatWeekSelectLabel = (date, invalidLabel) => {
        if (date === null) {
            return ''
        }

        return date && date.isValid() ?
            `${date.clone().startOf('week').format('MMM DD')} - ${date.clone().endOf('week').format('MMM DD')}`
            : invalidLabel
    }

    renderCustomDayForDateTime = (date, selectedDate, dayInCurrentMonth, dayComponent) => {
        const { classes } = this.props

        const dayClassName = classNames({
            [classes.customDayHighlight]: date.isSame(selectedDate, 'day'),
        })

        return (
            <div className={classes.dayWrapper}>
                {dayComponent}
                <div className={dayClassName} />
            </div>
        )
    }

    renderWrappedDefaultDay = (date, selectedDate, dayInCurrentMonth) => {
        const { classes } = this.props

        const startDate = selectedDate.clone().day(0).startOf('day')
        const endDate = selectedDate.clone().day(6).endOf('day')

        const dayIsBetween = (
            date.isSame(startDate) ||
            date.isSame(endDate) ||
            (date.isAfter(startDate) && date.isBefore(endDate))
        )

        const firstDay = date.isSame(startDate, 'day')
        const lastDay = date.isSame(endDate, 'day')

        const wrapperClassName = classNames({
            [classes.highlight]: dayIsBetween,
            [classes.firstHighlight]: firstDay,
            [classes.endHighlight]: lastDay,
        })

        const dayClassName = classNames(classes.day, {
            [classes.nonCurrentMonthDay]: !dayInCurrentMonth,
            [classes.highlightNonCurrentMonthDay]: !dayInCurrentMonth && dayIsBetween,
        })

        return (
            <div className={wrapperClassName}>
                <IconButton className={dayClassName}>
                    <span> {date.format('DD')} </span>
                </IconButton>
            </div>
        )
    }

    render() {
        const { selectedDate } = this.state

        return (
            <Fragment>
                <div className="picker">
                    <DatePicker
                        keyboard
                        value={selectedDate}
                        onChange={this.handleDateChange}
                        renderDay={this.renderWrappedDefaultDay}
                        labelFunc={this.formatWeekSelectLabel}
                    />
                </div>
            </Fragment>
        )
    }
}

export default connect()(withStyles(styles)(WeekPicker))
