import React, { Component } from 'react'
import { connect } from 'react-redux'

import PropTypes from 'prop-types'
import Autosuggest from 'react-autosuggest'
import match from 'autosuggest-highlight/match'
import parse from 'autosuggest-highlight/parse'
import TextField from 'material-ui/TextField'
import Paper from 'material-ui/Paper'
import { MenuItem } from 'material-ui/Menu'
import { withStyles } from 'material-ui/styles'

import { initSuggestion, resetSuggestion, selectSuggestion } from '../actions/autosuggest'
import { fetchJson } from '../actions/rest'

function renderInput(inputProps) {
    const { classes, autoFocus, autoSuggestLabel, value, ref, ...other } = inputProps

    return (
        <TextField
            autoFocus={autoFocus}
            label={autoSuggestLabel}
            margin="normal"
            fullWidth
            value={value}
            inputRef={ref}
            InputProps={{
                classes: {
                    input: classes.input,
                },
                ...other,
            }}
        />
    )
}

function renderSuggestionsContainer(options) {
    const { containerProps, children } = options

    return (
        <Paper {...containerProps} square>
            {children}
        </Paper>
    )
}

const styles = theme => ({
    container: {
        flexGrow: 1,
        position: 'relative',
    },
    suggestionsContainerOpen: {
        // position: 'absolute',
        marginTop: theme.spacing.unit,
        marginBottom: theme.spacing.unit * 3,
        left: 0,
        right: 0,
    },
    suggestion: {
        display: 'block',
    },
    suggestionsList: {
        margin: 0,
        padding: 0,
        listStyleType: 'none',
    },
    textField: {
        width: '100%',
    },
})

class EnhancedAutoSuggest extends Component {

    constructor(props) {
        super(props)

        this.filterSuggestions = this.filterSuggestions.bind(this)
        this.handleSuggestionsFetchRequested = this.handleSuggestionsFetchRequested.bind(this)
        this.renderSuggestion = this.renderSuggestion.bind(this)
    }

    state = {
        value: '',
        suggestions: [],
        selectedSuggestions: [],
        loading: true,
        items: []
    }

    componentDidMount() {
        const { autoSuggestKey, url } = this.props

        this.props.fetchSuggestionData(`enhancedAutoSuggest_${autoSuggestKey}`, url)
        this.props.resetSuggestions(autoSuggestKey)
    }

    componentWillReceiveProps(props) {
        const autoSuggestRest = props.rest[`enhancedAutoSuggest_${this.props.autoSuggestKey}`]
        if (autoSuggestRest.items) {
            this.setState({
                items: autoSuggestRest.items
            })
        }

        const selectedSuggestions = this.props.selectedSuggestions
        const autoSuggestValueKey = this.props.autoSuggestValueKey

        if (autoSuggestRest.items && selectedSuggestions && selectedSuggestions.length > 0) {
            const items = autoSuggestRest.items.filter(item => {
                return !selectedSuggestions.find(selected => selected[autoSuggestValueKey] === item[autoSuggestValueKey])
            })

            this.setState({
                selectedSuggestions: [...selectedSuggestions],
                items: items
            })

            this.props.initSuggestions(this.props.autoSuggestKey, selectedSuggestions)
        }
    }

    handleSuggestionsFetchRequested({ value }) {
        this.setState({
            suggestions: this.filterSuggestions(value),
        })
    }

    filterSuggestions(value) {
        const inputValue = value.trim().toLowerCase()
        const inputLength = inputValue.length

        if (inputLength === 0) {
            return []
        }

        let count = 0

        const autoSuggestValueKey = this.props.autoSuggestValueKey
        const selectedSuggestions = this.state.selectedSuggestions

        let suggestionsItems = this.state.items

        if (selectedSuggestions && selectedSuggestions.length > 0) {
            suggestionsItems = [...suggestionsItems.filter(item => {
                return !selectedSuggestions.find(selected => selected[autoSuggestValueKey] === item[autoSuggestValueKey])
            })]
        }

        return suggestionsItems.filter(suggestion => {
            const keep = count < 5 && suggestion[autoSuggestValueKey].toLowerCase().slice(0, inputLength) === inputValue
            if (keep) {
                count += 1
            }
            return keep
        })
    }

    renderSuggestion(suggestion, { query, isHighlighted }) {
        const autoSuggestValueKey = this.props.autoSuggestValueKey
        const matches = match(suggestion[autoSuggestValueKey], query)
        const parts = parse(suggestion[autoSuggestValueKey], matches)

        return (
            <MenuItem selected={isHighlighted} component="div">
                <div>
                    {parts.map((part, index) => {
                        return part.highlight ? (
                            <span key={String(index)} style={{ fontWeight: 300 }}>
                                {part.text}
                            </span>
                        ) : (
                                <strong key={String(index)} style={{ fontWeight: 500 }}>
                                    {part.text}
                                </strong>
                            )
                    })}
                </div>
            </MenuItem>
        )
    }

    handleSuggestionsClearRequested = () => {
        this.setState({
            suggestions: [],
        })
    }

    handleChange = (event, { newValue }) => {
        this.setState({
            value: newValue
        })
    }

    render() {
        const { classes, autoSuggestKey, autoSuggestLabel } = this.props

        return (
            <Autosuggest
                theme={{
                    container: classes.container,
                    suggestionsContainerOpen: classes.suggestionsContainerOpen,
                    suggestionsList: classes.suggestionsList,
                    suggestion: classes.suggestion,
                }}
                renderInputComponent={renderInput}
                suggestions={this.state.suggestions}
                onSuggestionsFetchRequested={this.handleSuggestionsFetchRequested}
                onSuggestionsClearRequested={this.handleSuggestionsClearRequested}
                renderSuggestionsContainer={renderSuggestionsContainer}
                getSuggestionValue={(suggestion) => {
                    this.setState({
                        selectedSuggestions: [...this.state.selectedSuggestions, suggestion]
                    })
                    this.props.selectSuggestion(autoSuggestKey, suggestion)
                    return ''
                }}
                renderSuggestion={this.renderSuggestion}
                inputProps={{
                    autoFocus: true,
                    classes,
                    placeholder: autoSuggestLabel,
                    value: this.state.value,
                    onChange: this.handleChange,
                }}
            />
        )
    }
}

EnhancedAutoSuggest.propTypes = {
    classes: PropTypes.object.isRequired,
}

const mapStateToProps = (state) => ({
    rest: state.rest
})

const mapDispatchToProps = (dispatch) => {
    return {
        fetchSuggestionData: (key, url) => dispatch(
            fetchJson({ key, url })
        ),
        initSuggestions: (key, suggestions) => dispatch(
            initSuggestion({
                value: suggestions,
                key
            })
        ),
        selectSuggestion: (autoSuggestKey, suggestion) => {
            dispatch(
                selectSuggestion({
                    key: autoSuggestKey,
                    value: suggestion
                })
            )
        },
        resetSuggestions: (autoSuggestKey) => dispatch(
            resetSuggestion(autoSuggestKey)
        )
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles)(EnhancedAutoSuggest))
