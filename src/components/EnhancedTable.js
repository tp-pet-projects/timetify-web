import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { withStyles } from 'material-ui/styles'

import { FormControl } from 'material-ui/Form'
import Grid from 'material-ui/Grid'
import IconButton from 'material-ui/IconButton'
import Input, { InputLabel, InputAdornment } from 'material-ui/Input'
import Paper from 'material-ui/Paper'
import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui/Table'
import Typography from 'material-ui/Typography'

import AddIcon from 'material-ui-icons/Add'
import DeleteIcon from 'material-ui-icons/Delete'
import EditIcon from 'material-ui-icons/Edit'
import SearchIcon from 'material-ui-icons/Search'

import Loader from './Loader'

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
})


class EnhancedTable extends Component {

    render() {
        const pathname = window.location.pathname.split('/')[1]
        const { headline, columns, loading, rows = [], customActions = [], onClickDelete } = this.props


        return (
            <Paper>
                <div style={{ padding: '15px 20px 0px' }}>
                    <Grid container alignItems="center" direction="row" justify="space-between" >
                        <Grid item>
                            <Typography type="title">{headline}</Typography>
                        </Grid>
                        <Grid item>
                            <FormControl>
                                <InputLabel htmlFor="search">Search</InputLabel>
                                <Input id="search" endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton>
                                            <SearchIcon />
                                        </IconButton>
                                    </InputAdornment>
                                }
                                />
                            </FormControl>
                        </Grid>
                    </Grid>
                    <Grid container alignItems="center" direction="row" justify="flex-end" >
                        <Grid item>
                            <Link to={`/${pathname}/new/`}>
                                <IconButton aria-label="add">
                                    <AddIcon />
                                </IconButton>
                            </Link>
                        </Grid>
                        <Grid item>
                            <IconButton aria-label="Delete">
                                <DeleteIcon />
                            </IconButton>
                        </Grid>
                    </Grid>
                </div>



                <Table>
                    <TableHead>
                        <TableRow>
                            {columns.filter(col => col.label).map((col, index) => (
                                <TableCell key={index}>{col.label}</TableCell>
                            ))}
                            <TableCell key={columns + 1}>Actions</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {loading ?
                            <TableRow>
                                <TableCell colSpan={columns.length + 1}>
                                    <div style={{ paddingBottom: 15 }}>
                                        <Loader />
                                    </div>
                                </TableCell>
                            </TableRow>
                            :
                            rows.map((row, index) => {
                                const filteredCols = columns.filter(col => col.primaryKey)
                                let primaryId = ''

                                if (filteredCols && filteredCols.length === 1) {
                                    primaryId = row[columns.filter(col => col.primaryKey)[0].id]
                                }

                                return <TableRow key={index}>
                                    {columns.filter(col => col.label).map((col, colIndex) => (
                                        <TableCell key={colIndex}>{row[col.id]}</TableCell>
                                    ))}

                                    <TableCell key={columns.length + 1}>
                                        <Link to={`/${pathname}/${primaryId}/`}>
                                            <IconButton aria-label="Edit">
                                                <EditIcon />
                                            </IconButton>
                                        </Link>
                                        <IconButton aria-label="Delete"
                                            onClick={() => onClickDelete(row)}>
                                            <DeleteIcon />
                                        </IconButton>
                                        {customActions.map((customAction, customActionIndex) => (
                                            <IconButton key={customActionIndex}
                                                aria-label={customAction.label}
                                                onClick={() => customAction.onClick(primaryId)}>
                                                {customAction.icon}
                                            </IconButton>
                                        ))}
                                    </TableCell>
                                </TableRow>
                            })
                        }
                    </TableBody>
                </Table>
            </Paper>
        )
    }
}

EnhancedTable.propTypes = {
    columns: PropTypes.array.isRequired,
    rows: PropTypes.array.isRequired
}

export default withStyles(styles)(EnhancedTable)
