import React from 'react'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'react-router-redux'
import createHistory from 'history/createBrowserHistory'

import AppLayout from './containers/layouts'
import store from './store'

import './styles/_global.css'

const history = createHistory()
const App = () => {
    return (
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <AppLayout />
            </ConnectedRouter>
        </Provider>
    )
}

export default App
