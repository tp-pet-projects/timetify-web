import { AUTOSUGGEST_ACTIONS } from '../actions/autosuggest'

const INITIAL_STATE = {}

const autoSuggestReducer = (state = INITIAL_STATE, action) => {
    const payload = action.payload
    let suggestions

    switch (action.type) {
        case AUTOSUGGEST_ACTIONS.INIT_SUGGESTION:
            return {
                [payload.key]: [...payload.value]
            }
        case AUTOSUGGEST_ACTIONS.REMOVE_SUGGESTION:
            suggestions = state[payload.key]

            if (suggestions) {
                suggestions = suggestions.filter(value => value[payload.valueKey] !== payload.value)
            } else {
                suggestions = []
            }

            return {
                [payload.key]: [...suggestions]
            }
        case AUTOSUGGEST_ACTIONS.RESET_SUGGESTION:
            return {
                [payload.key]: []
            }
        case AUTOSUGGEST_ACTIONS.SELECT_SUGGESTION:
            suggestions = state[payload.key]

            if (!suggestions) {
                suggestions = []
            } else {
                suggestions = [...suggestions, payload.value]
            }

            return {
                [payload.key]: [...suggestions]
            }
        default: {
            return state
        }
    }
}

export default autoSuggestReducer
