import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import autoSuggestReducer from './autosuggest'
import restReducer from './rest'
import snackbarReducer from './snackbar'

import { COMMON_ACTIONS } from '../actions/common'

const commonReducer = (state = { message: '', dialog: {}, weekPicker: {} }, action) => {
    let payload = action.payload

    switch (action.type) {
        case COMMON_ACTIONS.TOGGLE_DIALOG:
            let dialogNextState = {}
            dialogNextState[payload.dialogId] = {
                ...payload
            }
            return {
                ...state,
                dialog: {
                    ...dialogNextState
                }
            }
        case 'SELECTED_WEEK':
            return {
                ...state,
                weekPicker: {
                    ...state.weekPicker,
                    [payload.key]: payload.date
                }
            }
        default:
            return state
    }
}

const rootReducer = combineReducers({
    autoSuggest: autoSuggestReducer,
    common: commonReducer,
    rest: restReducer,
    routerReducer,
    snackbar: snackbarReducer
})

export default rootReducer
