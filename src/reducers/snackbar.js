import { SNACKBAR_ACTIONS } from '../actions/snackbar'

const INITIAL_STATE = {
    open: false,
    message: ''
}

const snackbarReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case SNACKBAR_ACTIONS.SHOW_SNACKBAR:
            return {
                ...action.payload
            }
        case SNACKBAR_ACTIONS.HIDE_SNACKBAR:
            return {
                ...action.payload
            }
        default:
            return state
    }
}

export default snackbarReducer
