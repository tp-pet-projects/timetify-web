import { REST_ACTIONS } from '../actions/rest'

const INITIAL_STATE = {}

const rest = (state = INITIAL_STATE, action) => {
    let nextState = state
    let payload = action.payload
    switch (action.type) {
        case REST_ACTIONS.IS_LOADING:
            nextState[payload.key] = {
                loading: payload.data,
                ...nextState[payload.key]
            }

            return {
                ...nextState
            }
        case REST_ACTIONS.MAP_TO_ARRAY:
            nextState[payload.key] = {
                loading: false,
                items: payload.data
            }

            return {
                ...nextState
            }
        case REST_ACTIONS.MAP_TO_OBJECT:
            nextState[payload.key] = {
                loading: false,
                data: payload.data
            }

            return {
                ...nextState
            }
        default:
            return state
    }
}

export default rest
