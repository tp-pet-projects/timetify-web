import { deleteRecord } from './rest'

export const COMMON_ACTIONS = {
    TOGGLE_DIALOG: 'TOGGLE_DIALOG'
}

export function confirmDeletion(params = { key: '', dialogId: '', id: '', data: '', url: '', dispatch: null }) {
    const { dialogId, data, dispatch } = params
    return toggleDialog({
        dialogId: dialogId,
        open: true,
        title: 'Confirm Deletion',
        description: `Are you sure you want to delete [${data}]?`,
        okCallback: () => dispatch(deleteRecord({ ...params }))
    })
}

export function toggleDialog(dialog = { dialogId: '', open: false, description: '', title: '' }) {
    return {
        type: COMMON_ACTIONS.TOGGLE_DIALOG,
        payload: dialog
    }
}

export function setSelectedDate(key, date) {
    return {
        type: 'SELECTED_WEEK',
        payload: { key, date }
    }
}
