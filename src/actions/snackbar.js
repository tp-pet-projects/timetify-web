export const SNACKBAR_ACTIONS = {
    HIDE_SNACKBAR: 'HIDE_SNACKBAR',
    SHOW_SNACKBAR: 'SHOW_SNACKBAR',
}

export function hideSnackbar() {
    return {
        type: SNACKBAR_ACTIONS.HIDE_SNACKBAR,
        payload: {
            open: false,
            message: ''
        }
    }
}

export function showRecordAdded() {
    return {
        type: SNACKBAR_ACTIONS.SHOW_SNACKBAR,
        payload: {
            open: true,
            message: 'New record has been added'
        }
    }
}

export function showRecordDeleted() {
    return {
        type: SNACKBAR_ACTIONS.SHOW_SNACKBAR,
        payload: {
            open: true,
            message: 'Record has been deleted successfully'
        }
    }
}

export function showRecordUpdated() {
    return {
        type: SNACKBAR_ACTIONS.SHOW_SNACKBAR,
        payload: {
            open: true,
            message: 'Record has been updated successfully'
        }
    }
}

export function showError() {
    return {
        type: SNACKBAR_ACTIONS.SHOW_SNACKBAR,
        payload: {
            open: true,
            message: 'Whoops! Something went wrong, please try again later.'
        }
    }
}

export function showSnackbar(message) {
    return {
        type: SNACKBAR_ACTIONS.SHOW_SNACKBAR,
        payload: {
            open: true,
            message: message
        }
    }
}
