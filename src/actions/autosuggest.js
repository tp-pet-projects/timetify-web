export const AUTOSUGGEST_ACTIONS = {
    INIT_SUGGESTION: 'INIT_SUGGESTION',
    REMOVE_SUGGESTION: 'REMOVE_SUGGESTION',
    RESET_SUGGESTION: 'RESET_SUGGESTION',
    SELECT_SUGGESTION: 'SELECT_SUGGESTION'
}

export function initSuggestion(param = { key: '', value: [] }) {
    return {
        type: AUTOSUGGEST_ACTIONS.INIT_SUGGESTION,
        payload: { ...param }
    }
}

export function removeSuggestion(param = { key: '', value: '', valueKey: '' }) {
    return {
        type: AUTOSUGGEST_ACTIONS.REMOVE_SUGGESTION,
        payload: { ...param }
    }
}


export function resetSuggestion(key) {
    return {
        type: AUTOSUGGEST_ACTIONS.RESET_SUGGESTION,
        payload: { key }
    }
}

export function selectSuggestion(param = { key: '', value: '' }) {
    return {
        type: AUTOSUGGEST_ACTIONS.SELECT_SUGGESTION,
        payload: { ...param }
    }
}
