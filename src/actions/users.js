import { ENDPOINTS, submit } from './rest'
import store from '../store'

export const USERS_ACTIONS = {
    ASSIGN_TO_CLIENT: 'ASSIGN_TO_CLIENT'
}

export function assignUserToClients(userId) {
    const clientIds = store.getState().common.autoSuggest.selectedClients.map(client => client.clientId)
    return submit({
        key: 'usersList',
        url: `${ENDPOINTS.USERS}/${userId}/assign`,
        data: clientIds,
        method: 'PUT'
    })
}
