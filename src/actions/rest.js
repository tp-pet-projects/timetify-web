import { showSnackbar, hideSnackbar, showRecordAdded, showRecordDeleted, showRecordUpdated, showError } from './snackbar'

export const REST_ACTIONS = {
    FETCH: 'FETCH',
    MAP_TO_ARRAY: 'MAP_TO_ARRAY',
    MAP_TO_OBJECT: 'MAP_TO_OBJECT',
    IS_LOADING: 'IS_LOADING'
}

const baseUrl = 'http://localhost:8080/api'

export const ENDPOINTS = {
    TOKEN: `${baseUrl}/auth/token`,
    PROFILE: `${baseUrl}/profile`,
    CLIENTS: `${baseUrl}/clients`,
    PROJECTS: `${baseUrl}/projects`,
    TIMESHEET: `${baseUrl}/timesheets`,
    USERS: `${baseUrl}/users`
}

export function fetchJson(params = { key: '', url: '' }) {
    const { key, url } = params
    const token = localStorage.getItem('token');

    if (!token) {
        return showSignIn(key)
    }

    return (dispatch) => {
        dispatch(hideSnackbar())
        dispatch(isLoading(key, true))
        fetch(url, {
            headers: {
                'Authorization': `Bearer ${JSON.parse(token).access_token}`,
                'Content-Type': 'application/json'
            }
        })
            .then((response) => handleResponse(dispatch, response, key))
            .then(response => response.json())
            .then((data) => {
                if (Array.isArray(data)) {
                    dispatch(mapToArray(key, data))
                } else {
                    dispatch(mapToObject(key, data))
                }
                dispatch(isLoading(key, false))

                if (params.chain) {
                    dispatch(params.chain)
                }
            })
            .catch(() => {
                dispatch(showError())
                dispatch(isLoading(key, false))
            })
    }
}

export function deleteRecord(params = { key: '', url: '', id: '' }) {
    const { key, url, id } = params
    const token = localStorage.getItem('token');

    if (!token) {
        return showSignIn(key)
    }

    return (dispatch) => {
        dispatch(hideSnackbar())
        dispatch(isLoading(key, true))
        fetch(`${url}/${id}`, {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${JSON.parse(token).access_token}`
            }
        })
            .then((response) => handleResponse(dispatch, response, key))
            .then(() => {
                dispatch(fetchJson({ ...params }))
                dispatch(showRecordDeleted())
                dispatch(isLoading(key, false))
            })
            .catch(() => {
                dispatch(showError())
                dispatch(isLoading(key, false))
            })
    }
}

export function submit(params = { key: '', url: '', data: '', method: 'POST' }) {
    const { key, url, data, method } = params
    const token = localStorage.getItem('token');

    if (!token) {
        return showSignIn(key)
    }

    return (dispatch) => {
        dispatch(hideSnackbar())
        fetch(url, {
            method: method,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${JSON.parse(token).access_token}`
            },
            body: JSON.stringify(data)
        })
            .then((response) => handleResponse(dispatch, response, key))
            .then(response => response.json())
            .then((data) => {
                dispatch(mapToObject(key, data))
                if (method === 'POST') {
                    dispatch(showRecordAdded())
                } else {
                    dispatch(showRecordUpdated())
                }

                if (params.chain) {
                    dispatch(params.chain)
                }
            }).catch(() => {
                dispatch(showError())
                dispatch(isLoading(key, false))
            })
    }
}

export function authenticate(data) {
    const key = 'signInKey'
    return (dispatch) => {
        dispatch(hideSnackbar())
        dispatch(isLoading(key, true))

        fetch(ENDPOINTS.TOKEN, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then((response) => {
            if (!response.ok) {
                dispatch(isLoading(key, false))
                throw Error(response.statusText)
            }
            return response
        })
            .then((response) => response.json())
            .then((response) => {
                localStorage.setItem('token', JSON.stringify(response));
                dispatch(mapToObject(key, response))
                dispatch(isLoading(key, false))
                window.location.href = '/'
            }).catch(() => {
                dispatch(showSnackbar('Whoops! Incorrect username or password.'))
                dispatch(isLoading(key, false))
            })
    }
}

export function isLoading(key, loading) {
    return {
        type: REST_ACTIONS.IS_LOADING,
        payload: {
            key: key,
            data: loading
        }
    }
}

export function mapToArray(key, list) {
    return {
        type: REST_ACTIONS.MAP_TO_ARRAY,
        payload: {
            key: key,
            data: list
        }
    }
}

export function mapToObject(key, object) {
    return {
        type: REST_ACTIONS.MAP_TO_OBJECT,
        payload: {
            key: key,
            data: object
        }
    }
}

export function showSignIn(key) {
    return (dispatch) => {
        dispatch(hideSnackbar())
        dispatch(isLoading(key, false))

        if (window.location.pathname !== '/sign-in/') {
            window.location.href = '/sign-in/'
        } else {
            if (localStorage.getItem('expired')) {
                dispatch(showSnackbar('Your session has expired. You must sign in again.'))
                localStorage.setItem('expired', '')
            }
        }
    }
}

function handleResponse(dispatch, response, key) {
    if (!response.ok) {
        if (response.type === 'cors' && response.status === 401) {
            if (window.location.pathname !== '/sign-in/') {
                localStorage.setItem('expired', true)
            }
            dispatch(showSignIn(key))
        } else {
            throw Error(response.statusText)
        }
    }

    return response
}
